﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doorway : MonoBehaviour
{
    bool activated = false;

    public Vector3 endPosition;
    public Transform Grate;
    public float speed = 1f;
    public AudioSource AudioSource;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(activated && Grate.localPosition != endPosition)
        {
            Grate.localPosition = Vector3.MoveTowards(Grate.localPosition, endPosition, Time.deltaTime * speed);
        }
    }

    public void OpenDoor()
    {
        gameObject.isStatic = false;
        activated = true;
        AudioSource.Play();
    }

}
