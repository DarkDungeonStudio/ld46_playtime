﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteract : MonoBehaviour
{
    public Camera Camera;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 2f))
        {
            Debug.Log(hit.transform.tag
                
                );
            if (hit.transform.CompareTag("Lever"))
            {
                // highlight lever
                Debug.Log("Lever");

                if (Input.GetMouseButtonDown(0))
                {
                    Debug.Log("Interact Lever");
                    Lever lever = hit.collider.GetComponent<Lever>();

                    // Interact
                    lever.Interact();
                }
            }
        }

        
    }
}
