﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Lever : MonoBehaviour
{
    public UnityEvent m_MyEvent;
    Animator Animator;
    public bool Down = false;

    // Start is called before the first frame update
    void Start()
    {
        Animator = GetComponent<Animator>();
    }

    public void Interact()
    {
        m_MyEvent.Invoke();
        Down = !Down;
        Animator.SetBool("Down", Down);
        Animator.SetTrigger("Action");
    }
}
