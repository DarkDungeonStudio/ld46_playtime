﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManger : MonoBehaviour
{
    public GameObject Options;
    public GameObject Main;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void Play()
    {
        SceneManager.LoadScene("Level1");
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void OptionsMenu()
    {
        Options.SetActive(true);
        Main.SetActive(false);
    }

    public void BackToMain()
    {
        Options.SetActive(false);
        Main.SetActive(true);
    }
}
