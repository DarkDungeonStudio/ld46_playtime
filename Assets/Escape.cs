﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Escape : MonoBehaviour
{
    public List<AudioClip> EscapeClips;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<AudioSource>().clip = EscapeClips[Random.Range(0, EscapeClips.Count)];
        GetComponent<AudioSource>().Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
