%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Ogre
  m_Mask: 01000000010000000100000000000000000000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: FAT_OGRE
    m_Weight: 1
  - m_Path: FAT_OGRE_
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ L Thigh
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ L Thigh/FAT_OGRE_ L Calf
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ L Thigh/FAT_OGRE_ L Calf/FAT_OGRE_
      L Foot
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ L Thigh/FAT_OGRE_ L Calf/FAT_OGRE_
      L Foot/FAT_OGRE_ L Toe0
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ R Thigh
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ R Thigh/FAT_OGRE_ R Calf
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ R Thigh/FAT_OGRE_ R Calf/FAT_OGRE_
      R Foot
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ R Thigh/FAT_OGRE_ R Calf/FAT_OGRE_
      R Foot/FAT_OGRE_ R Toe0
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      L Clavicle
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      L Clavicle/FAT_OGRE_ L UpperArm
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      L Clavicle/FAT_OGRE_ L UpperArm/FAT_OGRE_ L Forearm
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      L Clavicle/FAT_OGRE_ L UpperArm/FAT_OGRE_ L Forearm/FAT_OGRE_ L Hand
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      L Clavicle/FAT_OGRE_ L UpperArm/FAT_OGRE_ L Forearm/FAT_OGRE_ L Hand/FAT_OGRE_
      L Finger0
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      L Clavicle/FAT_OGRE_ L UpperArm/FAT_OGRE_ L Forearm/FAT_OGRE_ L Hand/FAT_OGRE_
      L Finger0/FAT_OGRE_ L Finger01
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      L Clavicle/FAT_OGRE_ L UpperArm/FAT_OGRE_ L Forearm/FAT_OGRE_ L Hand/FAT_OGRE_
      L Finger0/FAT_OGRE_ L Finger01/FAT_OGRE_ L Finger02
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      L Clavicle/FAT_OGRE_ L UpperArm/FAT_OGRE_ L Forearm/FAT_OGRE_ L Hand/FAT_OGRE_
      L Finger1
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      L Clavicle/FAT_OGRE_ L UpperArm/FAT_OGRE_ L Forearm/FAT_OGRE_ L Hand/FAT_OGRE_
      L Finger1/FAT_OGRE_ L Finger11
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      L Clavicle/FAT_OGRE_ L UpperArm/FAT_OGRE_ L Forearm/FAT_OGRE_ L Hand/FAT_OGRE_
      L Finger1/FAT_OGRE_ L Finger11/FAT_OGRE_ L Finger12
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      L Clavicle/FAT_OGRE_ L UpperArm/FAT_OGRE_ L Forearm/FAT_OGRE_ L Hand/FAT_OGRE_
      L Finger2
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      L Clavicle/FAT_OGRE_ L UpperArm/FAT_OGRE_ L Forearm/FAT_OGRE_ L Hand/FAT_OGRE_
      L Finger2/FAT_OGRE_ L Finger21
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      L Clavicle/FAT_OGRE_ L UpperArm/FAT_OGRE_ L Forearm/FAT_OGRE_ L Hand/FAT_OGRE_
      L Finger2/FAT_OGRE_ L Finger21/FAT_OGRE_ L Finger22
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      L Clavicle/FAT_OGRE_ L UpperArm/FAT_OGRE_ L Forearm/FAT_OGRE_ L Hand/FAT_OGRE_
      L Finger3
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      L Clavicle/FAT_OGRE_ L UpperArm/FAT_OGRE_ L Forearm/FAT_OGRE_ L Hand/FAT_OGRE_
      L Finger3/FAT_OGRE_ L Finger31
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      L Clavicle/FAT_OGRE_ L UpperArm/FAT_OGRE_ L Forearm/FAT_OGRE_ L Hand/FAT_OGRE_
      L Finger3/FAT_OGRE_ L Finger31/FAT_OGRE_ L Finger32
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      L Clavicle/FAT_OGRE_ L UpperArm/FAT_OGRE_ L Forearm/FAT_OGRE_ L Hand/FAT_OGRE_
      L Finger4
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      L Clavicle/FAT_OGRE_ L UpperArm/FAT_OGRE_ L Forearm/FAT_OGRE_ L Hand/FAT_OGRE_
      L Finger4/FAT_OGRE_ L Finger41
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      L Clavicle/FAT_OGRE_ L UpperArm/FAT_OGRE_ L Forearm/FAT_OGRE_ L Hand/FAT_OGRE_
      L Finger4/FAT_OGRE_ L Finger41/FAT_OGRE_ L Finger42
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      Neck
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      Neck/FAT_OGRE_ Head
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      Neck/FAT_OGRE_ Head/FAT_OGRE_ Ponytail1
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      Neck/FAT_OGRE_ Head/FAT_OGRE_EYE_L
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      Neck/FAT_OGRE_ Head/FAT_OGRE_EYE_R
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      R Clavicle
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      R Clavicle/FAT_OGRE_ R UpperArm
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      R Clavicle/FAT_OGRE_ R UpperArm/FAT_OGRE_ R Forearm
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      R Clavicle/FAT_OGRE_ R UpperArm/FAT_OGRE_ R Forearm/FAT_OGRE_ R Hand
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      R Clavicle/FAT_OGRE_ R UpperArm/FAT_OGRE_ R Forearm/FAT_OGRE_ R Hand/default001
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      R Clavicle/FAT_OGRE_ R UpperArm/FAT_OGRE_ R Forearm/FAT_OGRE_ R Hand/FAT_OGRE_
      R Finger0
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      R Clavicle/FAT_OGRE_ R UpperArm/FAT_OGRE_ R Forearm/FAT_OGRE_ R Hand/FAT_OGRE_
      R Finger0/FAT_OGRE_ R Finger01
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      R Clavicle/FAT_OGRE_ R UpperArm/FAT_OGRE_ R Forearm/FAT_OGRE_ R Hand/FAT_OGRE_
      R Finger0/FAT_OGRE_ R Finger01/FAT_OGRE_ R Finger02
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      R Clavicle/FAT_OGRE_ R UpperArm/FAT_OGRE_ R Forearm/FAT_OGRE_ R Hand/FAT_OGRE_
      R Finger1
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      R Clavicle/FAT_OGRE_ R UpperArm/FAT_OGRE_ R Forearm/FAT_OGRE_ R Hand/FAT_OGRE_
      R Finger1/FAT_OGRE_ R Finger11
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      R Clavicle/FAT_OGRE_ R UpperArm/FAT_OGRE_ R Forearm/FAT_OGRE_ R Hand/FAT_OGRE_
      R Finger1/FAT_OGRE_ R Finger11/FAT_OGRE_ R Finger12
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      R Clavicle/FAT_OGRE_ R UpperArm/FAT_OGRE_ R Forearm/FAT_OGRE_ R Hand/FAT_OGRE_
      R Finger2
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      R Clavicle/FAT_OGRE_ R UpperArm/FAT_OGRE_ R Forearm/FAT_OGRE_ R Hand/FAT_OGRE_
      R Finger2/FAT_OGRE_ R Finger21
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      R Clavicle/FAT_OGRE_ R UpperArm/FAT_OGRE_ R Forearm/FAT_OGRE_ R Hand/FAT_OGRE_
      R Finger2/FAT_OGRE_ R Finger21/FAT_OGRE_ R Finger22
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      R Clavicle/FAT_OGRE_ R UpperArm/FAT_OGRE_ R Forearm/FAT_OGRE_ R Hand/FAT_OGRE_
      R Finger3
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      R Clavicle/FAT_OGRE_ R UpperArm/FAT_OGRE_ R Forearm/FAT_OGRE_ R Hand/FAT_OGRE_
      R Finger3/FAT_OGRE_ R Finger31
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      R Clavicle/FAT_OGRE_ R UpperArm/FAT_OGRE_ R Forearm/FAT_OGRE_ R Hand/FAT_OGRE_
      R Finger3/FAT_OGRE_ R Finger31/FAT_OGRE_ R Finger32
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      R Clavicle/FAT_OGRE_ R UpperArm/FAT_OGRE_ R Forearm/FAT_OGRE_ R Hand/FAT_OGRE_
      R Finger4
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      R Clavicle/FAT_OGRE_ R UpperArm/FAT_OGRE_ R Forearm/FAT_OGRE_ R Hand/FAT_OGRE_
      R Finger4/FAT_OGRE_ R Finger41
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_ Spine/FAT_OGRE_ Spine1/FAT_OGRE_
      R Clavicle/FAT_OGRE_ R UpperArm/FAT_OGRE_ R Forearm/FAT_OGRE_ R Hand/FAT_OGRE_
      R Finger4/FAT_OGRE_ R Finger41/FAT_OGRE_ R Finger42
    m_Weight: 1
  - m_Path: FAT_OGRE_/FAT_OGRE_ Pelvis/FAT_OGRE_SAGGING_BELLY
    m_Weight: 1
