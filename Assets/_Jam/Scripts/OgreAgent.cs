﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;
using UnityStandardAssets.Characters.ThirdPerson;
using Random = UnityEngine.Random;

public class OgreAgent : MonoBehaviour
{
    public AudioSource head;
    public AudioSource footL;
    public AudioSource footR;

    public bool OnPatrol = true;
    public float PatrolCooldown = 5f;
    public List<Transform> PatrolPoints;

    public List<AudioClip> footSteps;
    public List<AudioClip> hurt;
    public List<AudioClip> searching;
    public List<AudioClip> spotted;
    public List<AudioClip> death;
    public List<AudioClip> hear;
    public List<AudioClip> caught;
    public List<AudioClip> elevator;
    public List<AudioClip> escape;
    public List<AudioClip> surprise;

    public Transform Player;
    private FirstPersonController PlayerController;

    private AICharacterControl AICharacterControl;
    private NavMeshAgent NavMeshAgent;
    private Animator Animator;

    private GameObject target;
    private bool footLeft = false;
    public float stepDelay = 1f;
    public float currentStepDelay = 0f;

    public float voiceDelay = 5f;
    public float currentVoiceDelay = 0f;

    public float grabDelay = 2f;
    public float currentGrabDelay = 0f;

    public float spottedDelay = 10f;
    public float currentSpottedDelay = 10f;

    public float hearDelay = 10f;
    public float currentHearDelay = 10f;

    public int Health = 5;

    public float sightDistance = 25f;
    public float rearDistance = 2f;
    public float hearDistance = 50f;

    public bool inElevator = false;

    public float walkRadius = 10f;

    public bool hearPlayer = false;
    public bool seePlayer = false;


    // Start is called before the first frame update
    void Start()
    {
        head.loop = false;
        footL.loop = false;
        footR.loop = false;

        PlayerController = Player.GetComponent<FirstPersonController>();
        AICharacterControl = GetComponent<AICharacterControl>();
        target = GameObject.Instantiate(new GameObject(), transform.position, Quaternion.identity);
        NavMeshAgent = GetComponent<NavMeshAgent>();
        Animator = GetComponent<Animator>();
        AICharacterControl.target = null;
    }

    void OnAnimatorMove()
    {
        if(NavMeshAgent)
            NavMeshAgent.velocity = Animator.deltaPosition / Time.deltaTime;
    }

    // Update is called once per frame
    void Update()
    {

        if (inElevator)
        {
            return;
        }

        if (Health <= 0)
        {
            Death();
            return;
        }

        if (!hearPlayer && !seePlayer && NavMeshAgent.remainingDistance < 2 && OnPatrol)
        {
            OnPatrol = false;
            Invoke("AIPatrol", PatrolCooldown);
            Debug.Log("GO");
        }

        if (seePlayer)
        {
            AICharacterControl.target = Player.transform;
            Debug.Log("Player SEE");
        }

        if (!PlayerController.m_InShadows)
        {
            //FRONT CHECK
            SeePlayerFront();


            // REAR CHECK
            SeePlayerRear();
        }
        else
        {
            seePlayer = false;
        }

        if (!PlayerController.m_IsWalking || PlayerController.m_Jumping)
        {
            if (currentHearDelay >= hearDelay)
            {
                PlayRandomClip(head, hear, true);
                currentHearDelay = 0;
            }

            SetTarget();

        }

        currentVoiceDelay += Time.deltaTime;

        if (currentGrabDelay < grabDelay)
        {
            currentGrabDelay += Time.deltaTime;
        }

        if (currentSpottedDelay < spottedDelay)
        {
            currentSpottedDelay += Time.deltaTime;
        }

        if (currentHearDelay < hearDelay)
        {
            currentHearDelay += Time.deltaTime;
        }

        if (currentVoiceDelay >= voiceDelay)
        {
            currentVoiceDelay = 0;
            PlayRandomClip(head, searching);
        }



        if (NavMeshAgent.velocity.magnitude > 0.1f)
        {
            currentStepDelay += Time.deltaTime;

            if (!footLeft && currentStepDelay >= stepDelay)
            {
                footL.pitch = Random.Range(0.5f, 1f);
                PlayRandomClip(footL, footSteps, true);
                footLeft = true;
                currentStepDelay = 0f;
            }
            else if (currentStepDelay >= stepDelay)
            {
                footR.pitch = Random.Range(0.5f, 1f);
                PlayRandomClip(footR, footSteps, true);
                footLeft = false;
                currentStepDelay = 0f;
            }
        }

        var distance = Vector3.Distance(transform.position, Player.position);

        if (distance <= 5f && (!PlayerController.m_HoldingBreath || !PlayerController.m_InShadows))
        {
            Attack();
        }
    }

    public void ForceIdle()
    {
        Animator.SetFloat("Forward", 0f);
    }

    void SeePlayerFront()
    {
        if (Vector3.Distance(transform.position, Player.position) <= sightDistance)
        {
            var sourceOrigin = transform.position + new Vector3(0, 3.5f, 0);
            var targetPos = Player.position + new Vector3(0, 0.5f, 0);
            // Does the ray intersect any objects excluding the player layer
            int skip1n2 = ~(1 << 8);
            if (Physics.Raycast(sourceOrigin, (targetPos - sourceOrigin), out RaycastHit hit, sightDistance, skip1n2))
            {
                Debug.DrawRay(sourceOrigin, (targetPos - sourceOrigin) * hit.distance, Color.red);

                if (hit.transform.CompareTag("Player"))
                {
                    if (CheckPlayerInfront())
                    {
                        if (currentSpottedDelay >= spottedDelay)
                        {
                            PlayRandomClip(head, spotted, true);
                            currentSpottedDelay = 0;
                        }

                        seePlayer = true;
                        return;
                    }
                }
            }
        }
        seePlayer = false;
    }

    void SeePlayerRear()
    {
        if (Vector3.Distance(transform.position, Player.position) <= rearDistance)
        {
            var sourceOrigin = transform.position + new Vector3(0, 1, 0);
            var targetPos = Player.position + new Vector3(0, 0.5f, 0);
            // Does the ray intersect any objects excluding the player layer
            if (Physics.Raycast(sourceOrigin, (targetPos - sourceOrigin), out RaycastHit hit, rearDistance))
            {
                Debug.DrawRay(sourceOrigin, (targetPos - sourceOrigin) * hit.distance, Color.red);
                Debug.Log("Did Hit");

                if (hit.transform.CompareTag("Player"))
                {
                    var lookat = Player.transform.position;
                    lookat.y = 0f;

                    transform.LookAt(lookat, Vector3.up);
                }
            }
        }
    }

    void PlayRandomClip(AudioSource source, List<AudioClip> audioClips, bool overload = false)
    {
        if (!source.isPlaying || overload)
        {
            source.Stop();
            source.clip = RandomClip(audioClips);
            source.Play();
        }
    }

    AudioClip RandomClip(List<AudioClip> audioClips)
    {
        return audioClips[Random.Range(0, audioClips.Count)];
    }

    void Attack()
    {
        if (!Animator.GetCurrentAnimatorStateInfo(1).IsName("attack3") && currentGrabDelay >= grabDelay && CheckPlayerInfront())
        {
            currentGrabDelay = 0;
            Animator.SetTrigger("Attack");
            PlayRandomClip(head, caught, true);
            Invoke("CatchPlayer", 1f);
        }
    }

    void CatchPlayer()
    {
        PlayerController.Health -= 1;
    }

    void Death()
    {
        if (!Animator.GetBool("Death"))
        {
            head.volume = 1f;
            head.spatialBlend = 0.5f;
            PlayRandomClip(head, death, true);
            Animator.SetLayerWeight(1, 0);
            Animator.SetBool("Death", true);
            Destroy(AICharacterControl);
            Destroy(NavMeshAgent);
            Destroy(GetComponent<ThirdPersonCharacter>());
            Destroy(GetComponent<Rigidbody>());
            Destroy(GetComponent<CapsuleCollider>());

            Invoke("GameOver", 5f);
        }
    }

    void GameOver()
    {
        SceneManager.LoadScene("GameOver");
    }

    public void Elevator()
    {
        inElevator = true;
        PlayRandomClip(head, elevator, true);
        Destroy(AICharacterControl);
        Destroy(NavMeshAgent);
        Destroy(GetComponent<ThirdPersonCharacter>());
        Destroy(GetComponent<Rigidbody>());
        Destroy(GetComponent<CapsuleCollider>());
    }

    public void Escaped()
    {
        PlayRandomClip(head, escape, true);
    }

    public void Surpries()
    {
        PlayRandomClip(head, surprise, true);
    }

    public void Hurt(int damage = 0)
    {
        Health -= damage;

        if (Health > 0)
        {
            PlayRandomClip(head, hurt, true);
            Animator.SetTrigger("Hit1");
        }
    }

    private bool CheckPlayerInfront()
    {
        Vector3 enemyVector = Player.position - transform.position;

        return Vector3.Angle(transform.forward, enemyVector) <= 135 * 0.5f;
    }

    void SetTarget()
    {
        target.transform.position = Player.position;

        AICharacterControl.target = target.transform;
    }

    void AIPatrol()
    {
        var count = PatrolPoints.Where(x => x.gameObject.activeSelf).ToList().Count;
        OnPatrol = true;
        AICharacterControl.target = PatrolPoints.Where(x=>x.gameObject.activeSelf).ToList()[Random.Range(0, count)];

        Debug.Log("GO 2");
    }
}
