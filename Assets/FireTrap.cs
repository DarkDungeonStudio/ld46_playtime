﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class FireTrap : MonoBehaviour
{
    public int damage = 2;

    public float cooldown = 10f;

    public List<ParticleSystem> Flames;

    AudioSource AudioSource;
    public AudioClip AudioClipSpring;

    bool enabled = true;
    bool triggered = false;

    FirstPersonController firstPersonController;

    // Start is called before the first frame update
    void Start()
    {
        AudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (triggered && enabled)
        {
            Invoke("SpikesDown", 5f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Trapped(other);
    }

    private void Trapped(Collider collision)
    {
        if (collision.transform.CompareTag("Ogre") && !triggered && enabled)
        {
            SpikesUp();
            collision.gameObject.GetComponent<OgreAgent>().Hurt(damage);
        }

        if (collision.transform.CompareTag("Player") && !triggered && enabled)
        {
            SpikesUp();
            firstPersonController = collision.gameObject.GetComponent<FirstPersonController>();
            Invoke("HurtPlayer", 0.5f);
        }
    }

    void HurtPlayer()
    {
        firstPersonController.Hurt(damage);
    }

    void SpikesUp()
    {
        Flames.ForEach(x => x.Play());
        AudioSource.clip = AudioClipSpring;
        AudioSource.Play();
        triggered = true;
    }

    void SpikesDown()
    {
        Flames.ForEach(x => x.Stop());
        AudioSource.Stop();
        triggered = false;
    }

    public void Interact()
    {
        enabled = !enabled;
    }
}
