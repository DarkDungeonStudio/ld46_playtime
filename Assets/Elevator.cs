﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityStandardAssets.Characters.ThirdPerson;

public class Elevator : MonoBehaviour
{
    public Transform Target;
    public Transform Ogre;
    private AICharacterControl OgreAI;
    private OgreAgent OgreAgent;
    private AudioSource AudioSource;

    public Vector3 bottom;
    public bool triggered;
    public float speed = 5f;
    public UnityEvent m_MyEvent;

    public float delayVal = 1.5f;
    public float TriggerDelay = 4f;

    // Start is called before the first frame update
    void Start()
    {
        OgreAI = Ogre.GetComponent<AICharacterControl>();
        OgreAgent = Ogre.GetComponent<OgreAgent>();
        bottom.x = transform.position.x;
        bottom.z = transform.position.z;
        AudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (triggered && transform.position != bottom)
        {
            transform.position = Vector3.MoveTowards(transform.position, bottom, Time.deltaTime * speed);

            if(!AudioSource.isPlaying)
            {
                AudioSource.Play();
                AudioSource.loop = true;
            }
        }else
        {
            AudioSource.Stop();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Ogre"))
        {
            OgreAgent.inElevator = true;
            Invoke("delay", delayVal);
        }
    }

    void delay()
    {
        OgreAgent.ForceIdle();
        Ogre.parent = transform;
        OgreAI.target = Target;
        OgreAgent.Elevator();
        triggered = true;
        Invoke("Trigger", TriggerDelay);
    }

    void Trigger()
    {
        m_MyEvent.Invoke();
    }
}
