﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour
{
    private void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void Yes()
    {
        SceneManager.LoadScene("Level1");
    }

    public void No()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
