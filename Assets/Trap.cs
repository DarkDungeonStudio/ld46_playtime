﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Trap : MonoBehaviour
{
    public int damage = 2;

    public float cooldown = 10f;

    public Transform Spikes;

    Vector3 startSpikes;
    Vector3 endSpikes;
    AudioSource AudioSource;
    public AudioClip AudioClipSpring;
    public AudioClip AudioClipLoad;

    bool enabled = true;
    bool triggered = false;

    // Start is called before the first frame update
    void Start()
    {
        startSpikes = Spikes.localPosition;

        var pos = startSpikes;
        pos.y += 5f;
        endSpikes = pos;

        AudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (triggered && enabled)
        {
            SpikesDown();
            // rest spikes
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Trapped(other);
    }

    private void Trapped(Collider collision)
    {
        if (collision.transform.CompareTag("Ogre") && !triggered && enabled)
        {
            SpikesUp();
            collision.gameObject.GetComponent<OgreAgent>().Hurt(damage);
        }

        if (collision.transform.CompareTag("Player") && !triggered && enabled)
        {
            SpikesUp();
            collision.gameObject.GetComponent<FirstPersonController>().Hurt(damage);
        }
    }

    void SpikesUp()
    {
        Spikes.localPosition = endSpikes;
        AudioSource.clip = AudioClipSpring;
        AudioSource.volume = 0.1f;
        AudioSource.Play();
        AudioSource.loop = false;
        triggered = true;
    }

    void SpikesDown()
    {
        if (Spikes.localPosition != startSpikes)
        {
            Spikes.localPosition = Vector3.MoveTowards(Spikes.localPosition, startSpikes, Time.deltaTime * cooldown);

            if (!AudioSource.isPlaying)
            {
                AudioSource.clip = AudioClipLoad;
                AudioSource.Play();
                AudioSource.loop = true;
                AudioSource.volume = 0.2f;
            }
        }
        else
        {
            AudioSource.Stop();
            triggered = false;
        }
    }

    public void Interact()
    {
        enabled = !enabled;
    }
}
